#!/usr/bin/env bash

set -ex

while getopts i:v: option; do
  case "${option}" in
    i) INSTALL=${OPTARG};;
    v) VERSION=${OPTARG};;
  esac
done

HASHICORP_URL=https://releases.hashicorp.com
HASHICORP_ARCH=linux_amd64

DOWNLOAD=${HASHICORP_URL}/${INSTALL}/${VERSION}/${INSTALL}_${VERSION}_${HASHICORP_ARCH}.zip
DOWNLOAD_SHA=${HASHICORP_URL}/${INSTALL}/${VERSION}/${INSTALL}_${VERSION}_SHA256SUMS

# Dependencies

## Install
echo "Installing dependencies..."
for deps in unzip curl; do
  yum list installed $deps >/dev/null 2>&1 || yum -y -q -e 0 install $deps
done

# Hashicorp

## Download
echo "Downloading ${INSTALL}..."
curl -sL $DOWNLOAD > ${INSTALL}_${VERSION}_${HASHICORP_ARCH}.zip

## Verify
echo "Verifing ${INSTALL}..."
curl -sL $DOWNLOAD_SHA > SHA256SUMS
grep ${INSTALL}_${VERSION}_${HASHICORP_ARCH}.zip SHA256SUMS|sha256sum --check -
rm -f SHA256SUMS

## Install
echo "Installing ${INSTALL}..."
unzip -qq ${INSTALL}_${VERSION}_${HASHICORP_ARCH}.zip -d /usr/local/bin
rm -f ${INSTALL}_${VERSION}_${HASHICORP_ARCH}.zip

# Permissions

## Setting
echo "Setting permissions..."
chmod 0755 /usr/local/bin/${INSTALL}
chown root:root /usr/local/bin/${INSTALL}
